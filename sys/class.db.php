<?php
/*
* 諸々データを加工するclass
*/
class DB
{

    var $_db;

    public function FixcontractDate($contract_km_date_end, $contract_sns_date_end)
    {
        if(is_null($contract_km_date_end)){
            $contract_km_date_end = "なし";
        }else if(is_null($contract_sns_date_end)){
            $contract_sns_date_end = "なし";
        }
        return [$contract_km_date_end, $contract_sns_date_end];
    }
    
    public function productDiscrimination($seo, $sns, $km_or_kme)
    {
        if($seo == 1){
            if($km_or_kme == 1 && $sns == 1){
                $product = "KME,SNS";
            }else if($km_or_kme == 1){
                $product = "KME";
            }else if($sns == 1){
                $product = "KM,SNS";
            }else{
                $product = "KM";
            }
        }else if($sns == 1){
            $product = "SNS";
        }else{
            $product = "不明";
        }
        return $product;
    }

    public function contractTypeDiscrimination($seo, $contractType)
    {
        if($seo == 0){
            $contractType = "-";
            return $contractType;
        }
        if($contractType == 0){
            $contractType = "ライト";
        }else if($contractType == 1){
            $contractType = "スタンダード";
        }else if($contractType == 2){
            $contractType = "エキスパート";
        }else if($contractType == 3){
            $contractType = "スターター";
        }else{
            $contractType = "不明";
        }
        return $contractType;
    }

    public function defineActiveUser($user_count_seo, $user_count_sns, $login_bwsr)
    {
        if($user_count_seo == 0){
            $active_user_seo = $login_bwsr;
        }else{
            $active_user_seo = $user_count_seo;
        }
        
        if($user_count_sns == 0){
            $active_user_sns = $login_bwsr;
        }else{
            $active_user_sns = $user_count_sns;
        }
        return [$active_user_seo, $active_user_sns];
    }

    public function getweekNumAMonth()
    {
        $today = date('Y-m-d');     
        $WeekNum = intval(date('w', strtotime($today)));
        $j = intval(date('j', strtotime($today)));
        $WeekEndDay = $WeekNum != 6 ? (6 - $WeekNum) + $j : $j;
        return (int)ceil($WeekEndDay/7);
    }

    public function getweekday()
    {
        $yesterday = date('n/j', strtotime('-6 day'));
        $lastweek = date('n/j', strtotime('-12 day'));

        //$yesterday = date('n/j', strtotime('-1 day'));
        //$lastweek = date('n/j', strtotime('-7 day'));


        $result = $lastweek." - ".$yesterday;
        return $result;
    }

    public function processData($values)
    {
        $result = [];
        $product_count = 0;
        $score_count = 0;
        foreach($values as $index => $column){ //$column = ヘルススコア、月次利用数の最大値と最小値
            foreach($column as $score){
                if(preg_match('/最/',$score)){
                    $score_count = 1;
                    $product_count ++;
                }
                if(is_numeric($score)){
                    $score = str_replace(',', '', $score);
                    $result[PRODUCT[$product_count]][MAX_MIN[$index]][$score_count] = $score;
                    $score_count ++;
                }
            }
            $product_count = 0;
            $score_count = 0;
        }
        return $result;
    }
    
}