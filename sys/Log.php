<?php

require_once __DIR__ . "/../vendor/autoload.php";

use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\SlackWebhookHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryUsageProcessor;

/**
 * Class Log
 */
class Log extends Logger
{
    /**
     * @var Logger
     */
    private static $logger;
    
    /***
     * Log constructor.
     * @param string $name
     * @param array $handlers
     * @param array $processors
     * @param DateTimeZone|null $timezone
     */
    private function __construct(
      string $name,
      array $handlers = [],
      array $processors = [],
      ?DateTimeZone $timezone = null
    ) {
        try {
            parent::__construct($name, $handlers, $processors, $timezone);
            self::$logger = new Logger($name, $handlers, $processors, $timezone);
            $fileHandler = new RotatingFileHandler(__DIR__ . '/../log/app.log', 7, Logger::INFO);
            $slackHandler = new SlackWebhookHandler(
              SLACK_WEBHOOK_URL,
              SLACK_WEBHOOK_CHANNEL,
              SLACK_WEBHOOK_USERNAME,
              true,
              SLACK_WEBHOOK_ICON_EMOJI,
              true,
              true,
              Logger::ERROR
            );
            self::$logger->pushHandler($fileHandler);
            self::$logger->pushHandler($slackHandler);
            self::$logger->pushProcessor(new MemoryUsageProcessor);
            self::$logger->pushProcessor(new IntrospectionProcessor);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
    
    /**
     * @return Logger
     */
    public static function getInstance()
    {
        if (!isset(self::$logger)) {
            new Log('app');
        }
        return self::$logger;
    }
}
