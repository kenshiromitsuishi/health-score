<?php
require("./config.php");

try{
    $range = array(
      'ranges' => [
        DEV_LOG.'!A1:AH1', //DEV_LOGの日付（colmunとして取得する）
        DEV_LOG.'!B:C' //DEV_LOGのユーザーIDと企業名
      ]
    );

    $values = $_spreadsheet->batchGet($range);

    foreach($values[0]->values[0] as $index => $column){ //DEV_LOGにて、本日の日付の列アルファベットを取得
      if($column == CURRENT_DATE_SS){
        $alphabet = CONVERT_ALPHA_NUMRIC[$index]; //前日の日付に出力する
        break;
      }
    }

    //dbより
    $logs = $_actionLogger->getDailyLog(); //mongodbより、ユーザーIDとアクセス数をとってくる
    foreach($values[1]->values as $index => $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($logs as $log){ //mongoより、ユーザーIDとアクセス数
            if($column[0] == $log["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
               //スプシに出力
               $_logger->info("アクセスログ出力企業名：", [$column[1]]);
               var_dump("アクセスログ出力企業名：".$column[1]."：".$log["access_count"]);
               $values = array([$log["access_count"]]);
               $_spreadsheet->outputAccessUPDATE(DEV_LOG, $values, $alphabet, $index+1);
              }
          }
      }
      $_logger->info("アクセスログ出力完了");
      var_dump("アクセスログ出力完了");
} catch(Exception $e){
   var_dump("エラーでやんす：".$e);
}

?>
