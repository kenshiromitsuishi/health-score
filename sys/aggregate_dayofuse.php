<?php
require("./config.php");

try{

    //変更すべき箇所
    $DB = SNS_DB;
    $SUMMARY = SNS_SUMMARY;
    
    //$logs = $_actionLogger_km->get_dayofuse_km(); //mongodbより、ユーザーIDとアクセス数をとってくる
    $logs = $_actionLogger_sns->get_dayofuse_sns(); //mongodbより、ユーザーIDとアクセス数をとってくる

    //週次集計対象のカラム取得
    $range = array(
      'ranges' => [
        $SUMMARY.'!A1:AH1', //SUMMARYのタイトル（colmunとして取得する）
        $DB.'!A:B', //DEV_LOGの企業ID, ユーザーID
        $SUMMARY.'!A:A', //DEV_SUMMARYの企業ID
      ]
    );
    $values = $_spreadsheet->batchGet($range);
    foreach($values[0]->values[0] as $index => $column){ //DEV_SUMMARYにて、「月次利用日数」のタイトルを取得
      if($column == SUMMARY_TITLE["月次利用日数"]){
        $alphabet = CONVERT_ALPHA_NUMRIC[$index+1]; //月次利用日数のセルを取得する
      }
    }

    //var_dump($alphabet);
    //exit;

    //企業ごとのまとめ month
    $results = [];
    foreach($values[1]->values as $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($logs as $log){ //mongoより、ユーザーIDとアクセス数
            if($column[1] == $log["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
                if( $results[$column[0]] <= $log["dayofuse"]){
                    $results[$column[0]] = $log["dayofuse"];
                }
              }
          }
      }
      

    //SUMMARYシートに出力 month
    foreach($values[2]->values as $index => $column){ //$column = DEV_SUMMARYの企業ID
      foreach($results as $contract_group_id => $dayofuse){
        if($column[0] == $contract_group_id){
          $row = $index+1;
          $data[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet}{$row}",
            'values' => [[$dayofuse]]
          ]);
          var_dump("企業ID：".$contract_group_id);
        }
      }
    }

    $_spreadsheet->outputAccessBATCH($data);
    var_dump("アクセスログ出力完了");
    
} catch(Exception $e){
   var_dump("エラーでやんす：".$e);
}

?>
