<?php

class ActionLog
{
    private $client;
    private $collection;

    public function __construct(
        string $authenticationDB,
        string $collection,
        string $username,
        string $password,
        ?string $host,
        ?int $port = 27017
      ) {
          try {
              $this->collection = $collection;
              $this->client = new MongoDB\Driver\Manager("mongodb://{$host}:{$port}/{$authenticationDB}", [
                'username' => $username,
                'password' => $password,
              ]);
          } catch (Exception $e) {
              throw $e;
          }
    }
    
    public function getDailyLog()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date'  => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date' => YESTERDAY_DATE,
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'date' => '$date',
                            'km_id' => '$km_id'],
                        'access_count' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'access_count' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand('snsm', $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['access_count'] = $document->access_count;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラーでやんす：".$e);
         }
    }

    public function getWeeklyLog_km()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$dateTime', 0, 10 ]],
                        'km_id' => '$userInfo.id']],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => "2022-05-30", //LAST_WEEK 
                            '$lte' => "2022-06-05"] //YESTERDAY_DATE
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id'],
                        'access_count' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'access_count' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_KM, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['access_count'] = $document->access_count;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    
    public function getWeeklyLog_sns()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => "2022-05-30", //LAST_MONDAY
                            '$lte' => "2022-06-05"], //LAST_SUNDAY
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id'],
                        'access_count' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'access_count' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_SNS, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['access_count'] = $document->access_count;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラーでやんす：".$e);
         }
    }

    public function getMonthlyLog_km()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$dateTime', 0, 10 ]],
                        'km_id' => '$userInfo.id']],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY] //THIS_MONTH
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id'],
                        'access_count' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'access_count' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_KM, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['access_count'] = $document->access_count;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function getMonthlyLog_sns()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY], //THIS_MONTH
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id'],
                        'access_count' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'access_count' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_SNS, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['access_count'] = $document->access_count;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function getActiveUsers_km_1()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$dateTime', 0, 10 ]],
                        'km_id' => '$userInfo.id']],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY] //THIS_MONTH
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id']]],[
                    '$project' => [
                        '_id' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_KM, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function getActiveUsers_km_2()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$dateTime', 0, 10 ]],
                        'km_id' => '$userInfo.id']],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY] //THIS_MONTH
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id',
                            'ip_address' => '$ip_address']]],[
                    '$project' => [
                        '_id' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_KM, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['ip_address'] = $document->_id->ip_address;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function getActiveUsers_sns_1()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY], //THIS_MONTH
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id']]],[
                    '$project' => [
                        '_id' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_SNS, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function getActiveUsers_sns_2()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY], //THIS_MONTH
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id',
                            'ip_address' => '$ip_address']]],[
                    '$project' => [
                        '_id' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_SNS, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id->km_id;
                $result[$index]['ip_address'] = $document->_id->ip_address;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function get_dayofuse_km()
    {
        try{
            //var_dump(LAST_MONTH);
            //var_dump(TODAY);
            //exit;
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$dateTime', 0, 10 ]],
                        'km_id' => '$userInfo.id']],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY] //THIS_MONTH
                        ]],[
                    '$group' => [
                        '_id' => [
                            'date' => '$date_string',
                            'km_id' => '$km_id']]],[
                    '$group' => [
                        '_id' => '$_id.km_id',
                        'dayofuse' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'dayofuse' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_KM, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id;
                $result[$index]['dayofuse'] = $document->dayofuse;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }

    public function get_dayofuse_sns()
    {
        try{
            $command = new MongoDB\Driver\Command([ 
                'aggregate' => $this->collection,
                'pipeline' => [[
                    '$addFields' => [
                        'date_string' => [
                            '$substr' => [
                                '$created_at', 0, 10 ]],
                        'km_id' => [
                            '$substr' => [
                                '$km_user_id', 0, 7 ]]]],[
                    '$match' => [
                        'date_string' => [
                            '$gte' => LAST_MONTH, //LAST_MONTH
                            '$lte' => TODAY], //THIS_MONTH
                        'path' => [
                            '$regex' => PATH,
                            '$options' => "x"
                        ]
                        ]],[
                    '$group' => [
                        '_id' => [
                            'km_id' => '$km_id',
                            'date' => '$date_string']]],[
                    '$group' => [
                        '_id' => '$_id.km_id',
                        'dayofuse' => [
                            '$sum' => 1]]],[
                    '$project' => [
                        '_id' => 1,
                        'dayofuse' => 1]],[
                    '$sort' => [
                        '_id' => 1]],
                ],
                'cursor' => new stdClass,
            ]);

            $cursor = $this->client->executeCommand(ACTION_LOG_AUTHENTICATION_DB_SNS, $command);
            $result = [];
            foreach ($cursor as $index => $document) {
                $result[$index]['id'] = $document->_id;
                $result[$index]['dayofuse'] = $document->dayofuse;
            }
            return $result;
        }catch(Exception $e){
            var_dump("mongoのエラー：".$e);
         }
    }
}
