<?php
require("./config.php");

try{

    //変更すべき箇所
    $SUMMARY = SNS_SUMMARY;
    $PRODUCT = PRODUCT[3];

    $range = array(
      'ranges' => [
        SCORE.'!C4:D', //ヘルススコア、月次利用数
        $SUMMARY.'!A3:A', //SUMMARYの企業ID
        $SUMMARY.'!W3:W', //SUMMARYの月次利用数
        SCORE.'!E4:F', //ヘルススコア、アクティブユーザー数
        $SUMMARY.'!S3:S' //SUMMARYのアクティブユーザー数(数②のIPアドレスの方)
      ],
      'majorDimension' => 'COLUMNS'
    );
    $range_rows = array(
      'ranges' => [
        $SUMMARY.'!A1:Z1', //SUMMARYのタイトル（colmunとして取得する）
      ]
    );

    $values = $_spreadsheet->batchGet($range);
    $values_rows = $_spreadsheet->batchGet($range_rows);
    $scores = $_db->processData($values[0]);
    $scores_activeUsers = $_db->processData($values[3]);

    foreach($values_rows[0]->values[0] as $index => $column){ //DEV_LOGにて、本日の日付の列アルファベットを取得
      if($column == SUMMARY_TITLE["月次利用数"]){
        $alphabet = CONVERT_ALPHA_NUMRIC[$index+2]; //SCOREのセルを取得する
      }
      if($column == SUMMARY_TITLE["アクティブユーザー数"]){
        $alphabet_activeUsers = CONVERT_ALPHA_NUMRIC[$index+6]; //数①(回)のセルを取得する
      }
    }

    $month_score = [];
    array_push($month_score, $values[1]->values[0]);
    array_push($month_score, $values[2]->values[0]);

    $month_activeUsers = [];
    array_push($month_activeUsers, $values[1]->values[0]);
    array_push($month_activeUsers, $values[4]->values[0]);

    //var_dump($alphabet_activeUsers);
    //var_dump($alphabet);
    //var_dump($scores_activeUsers[$PRODUCT]["最大"]);
    //exit;

    foreach($month_score[1] as $index => $access_log){ //$column = SUMMARYのアクセスログ 
      for($i = 1; $i <= count($scores[$PRODUCT]["最大"])+1; $i++){
        $access_log = str_replace(',', '', $access_log);
        $access_log = str_replace('%', '', $access_log);
        if($access_log != "" && $access_log != "0" && $i == 5){
          $row = $index+3;
          $data[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet}{$row}",
            'values' => [[5]]
          ]);
          var_dump("企業ID：".$month_score[0][$index]);
          break;
        }
        if($access_log != "" && $scores[$PRODUCT]["最大"][$i-1] <= $access_log && $scores[$PRODUCT]["最大"][$i] >= $access_log){
          $sco = array_search($scores[$PRODUCT]["最大"][$i], $scores[$PRODUCT]["最大"]);
          $row = $index+3;
          $data[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet}{$row}",
            'values' => [[$sco]]
          ]);
          var_dump("企業ID：".$month_score[0][$index]);
          break;
        }
      }
    }

    foreach($month_activeUsers[1] as $index => $access_log){ //$column = SUMMARYのアクセスログ 
      for($i = 1; $i <= count($scores_activeUsers[$PRODUCT]["最大"])+1; $i++){
        $access_log = str_replace(',', '', $access_log);
        if($i == 5){
          $row = $index+3;
          $data_activeUsers[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet_activeUsers}{$row}",
            'values' => [[5]]
          ]);
          var_dump("企業ID：".$month_activeUsers[0][$index]);
          break;
        }
        if($access_log != "" && $scores_activeUsers[$PRODUCT]["最大"][$i-1] <= $access_log && $scores_activeUsers[$PRODUCT]["最大"][$i] >= $access_log){
          $sco = array_search($scores_activeUsers[$PRODUCT]["最大"][$i], $scores_activeUsers[$PRODUCT]["最大"]);
          $row = $index+3;
          $data_activeUsers[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet_activeUsers}{$row}",
            'values' => [[$sco]]
          ]);
          var_dump("企業ID：".$month_activeUsers[0][$index]);
          break;
        }
      }
    }


    //$_spreadsheet->outputAccessBATCH($data);
    $_spreadsheet->outputAccessBATCH($data_activeUsers);
    var_dump("アクセスログ出力完了");

  } catch(Exception $e){
    var_dump("エラーでやんす：".$e);
  }

?>
