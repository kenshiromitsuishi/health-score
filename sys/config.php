<?php
require_once __DIR__ . "/../vendor/autoload.php";

use Dotenv\Dotenv;
$env = Dotenv::createImmutable(__DIR__ . '/..');
$env->load();

error_reporting(0);
ini_set("display_errors", 0);
error_reporting(E_ERROR);

mb_internal_encoding('UTF-8');
header('X-FRAME-OPTIONS: DENY');
date_default_timezone_set('Asia/Tokyo');
setlocale(LC_ALL, 'ja_JP.UTF-8');
mb_language("Japanese");
header('Content-type: text/html; charset=UTF-8');
header('Content-Language: ja');

//日付
define("CURRENT_DATE_SS", date("n/j"));
define("LAST_MONDAY_SS", date("n/j", strtotime("last Monday")));
define("YESTERDAY_DATE", date("Y-m-d", strtotime("-1 days")));
define("LAST_MONDAY", date("Y-m-d", strtotime("last Monday")));
define("LAST_SUNDAY", date("Y-m-d", strtotime("last Sunday")));
define("LAST_MONTH", date("Y-m-d", strtotime("-1 month")));
define("LAST_WEEK", date("Y-m-d", strtotime("-1 week")));
define("TODAY", date("Y-m-d"));
//define("CURRENT_DATE_SS", date("n/j", strtotime("-1 days")));
//define("YESTERDAY_DATE", date("Y-m-d", strtotime("-2 days")));

define("CURRENT_TIMESTAMP", date("Y-m-d H:i:s"));

// Keywordmapdb
define('KMDB_HOST',     $_ENV['KMDB_HOST']);
define('KMDB_NAME',     $_ENV['KMDB_NAME']);
define('KMDB_USER',     $_ENV['KMDB_USER']);
define('KMDB_PASSWORD', $_ENV['KMDB_PASSWORD']);

// sns-service-db
define('SNS_SERVICE_DB_HOST',     $_ENV['SNS_SERVICE_DB_HOST']);
define('SNS_SERVICE_DB_NAME',     $_ENV['SNS_SERVICE_DB_NAME']);
define('SNS_SERVICE_DB_USER',     $_ENV['SNS_SERVICE_DB_USER']);
define('SNS_SERVICE_DB_PASSWORD', $_ENV['SNS_SERVICE_DB_PASSWORD']);

// Mongo KM
define("ACTION_LOG_AUTHENTICATION_DB_KM", $_ENV['ACTION_LOG_AUTHENTICATION_DB_KM']);
define("ACTION_LOG_COLLECTION_NAME_KM",   $_ENV['ACTION_LOG_COLLECTION_NAME_KM']);
define("ACTION_LOG_USER_NAME_KM",         $_ENV['ACTION_LOG_USER_NAME_KM']);
define("ACTION_LOG_PASSWORD_KM",          $_ENV['ACTION_LOG_PASSWORD_KM']);
define("ACTION_LOG_HOST_KM",              $_ENV['ACTION_LOG_HOST_KM']);
define("ACTION_LOG_PORT_KM",              $_ENV['ACTION_LOG_PORT_KM']);

// Mongo SNS
define("ACTION_LOG_AUTHENTICATION_DB_SNS", $_ENV['ACTION_LOG_AUTHENTICATION_DB_SNS']);
define("ACTION_LOG_COLLECTION_NAME_SNS",   $_ENV['ACTION_LOG_COLLECTION_NAME_SNS']);
define("ACTION_LOG_USER_NAME_SNS",         $_ENV['ACTION_LOG_USER_NAME_SNS']);
define("ACTION_LOG_PASSWORD_SNS",          $_ENV['ACTION_LOG_PASSWORD_SNS']);
define("ACTION_LOG_HOST_SNS",              $_ENV['ACTION_LOG_HOST_SNS']);
define("ACTION_LOG_PORT_SNS",              $_ENV['ACTION_LOG_PORT_SNS']);

define("PATH", 
    "^(?!.*api)| #api以外のpath
    (?=.*api\/(
        dashboard\/get\/(twittertrenddetail | eventcalendardetail | toptweets | tweetfromtwaccount | tweetfromkwmonitoring)| #ダッシュボード
        twtrendwords\/(events | detail)| #イベントカレンダー
        socialposting\/(get\/list | add\/socialpost)| #投稿管理
        twaccount\/get\/(tweetcategory | tweettype | tweethashtag | tweettime | tweetlength | tweetkanji | tweetnewline | tweeturl)| #運用アカウント分析
        fullrtcp\/get\/(summary | lotterystate)| #キャンペーン分析
        documents\/get\/documentslist| #レポート作成
        twinfluencer\/detail| #アカウント抽出
        twadsearch\/get\/tweet #広告検索β
    )).*$");

// Spread Sheet シート名
// 共通
define("SPREADSHEET_ID", $_ENV['SPREADSHEET_ID']);
define("SCORE", "ヘルススコア数値");

// DEV
define("DEV_DB", "DEV_DB");
define("DEV_SUMMARY", "DEV_SUMMARY");
define("DEV_SCORE", "DEV_SCORE");
define("DEV_LOG", "DEV_LOG");

// PROD
define("ALL_DB", "紐付けデータ");
define("KM_DB", "KM_企業一覧");
define("KME_DB", "KME_企業一覧");
define("SNS_DB", "SNS_企業一覧");

define("KM_SUMMARY", "KM_SUMMARY");
define("KME_SUMMARY", "KME_SUMMARY");
define("SNS_SUMMARY", "SNS_SUMMARY");
define("SCORE", "SCORE");
define("LOG", "LOG");

define("PRODUCT", array(
    1 => "KM",
    2 => "KME",
    3 => "SNS"
));

define("MAX_MIN", array(
    0 => "最小",
    1 => "最大"
));

define("CONVERT_ALPHA_NUMRIC", array(
    "1" => "A",
    "2" => "B",
    "3" => "C",
    "4" => "D",
    "5" => "E",
    "6" => "F",
    "7" => "G",
    "8" => "H",
    "9" => "I",
    "10" => "J",
    "11" => "K",
    "12" => "L",
    "13" => "M",
    "14" => "N",
    "15" => "O",
    "16" => "P",
    "17" => "Q",
    "18" => "R",
    "19" => "S",
    "20" => "T",
    "21" => "U",
    "22" => "V",
    "23" => "W",
    "24" => "X",
    "25" => "Y",
    "26" => "Z",
    "27" => "AA",
    "28" => "AB",
    "29" => "AC",
    "30" => "AD",
    "31" => "AE",
    "32" => "AF",
    "33" => "AG",
    "34" => "AH",
    "35" => "AI",
    "36" => "AJ",
    "37" => "AK",
    "38" => "AL",
    "39" => "AM",
    "40" => "AN",
    "41" => "AO",
    "42" => "AP",
    "43" => "AQ",
    "44" => "AR",
    "45" => "AS",
    "46" => "AT",
    "47" => "AU",
    "48" => "AV",
    "49" => "AW",
    "50" => "AX",
    "51" => "AY",
    "52" => "AZ",
  ));

  define("SUMMARY_TITLE", array(
    1 => "4/4 - 10",
    2 => "4/11 - 17",
    3 => "4/18 - 24",
    4 => "4/25 - 5/1",
    5 => "5/2 - 5/8",
    "月次利用数" => "月次利用数",
    "月次利用率" => "月次利用率",
    "月次利用日数" => "月次利用日数",
    "アクティブユーザー数" => "アクティブユーザー数"
  ));

//読み込み
require("class.db.php");
require("class.keywordmapdb.php");
require("class.spreadsheet.php");
require("ActionLog.php");
require("Log.php");

//LOG
$_logger = Log::getInstance();

try{
    //class.db
    $_db = new DB;

    //keywordmapDB
    $_kmdb = new KeywordmapDB(KMDB_HOST, KMDB_NAME, KMDB_USER, KMDB_PASSWORD);

    //spreadsheet
    $_spreadsheet = new SpreadSheet();

    // Mongos
    $_actionLogger_km = new ActionLog(ACTION_LOG_AUTHENTICATION_DB_KM, ACTION_LOG_COLLECTION_NAME_KM, ACTION_LOG_USER_NAME_KM,ACTION_LOG_PASSWORD_KM, ACTION_LOG_HOST_KM, ACTION_LOG_PORT_KM);
    $_actionLogger_sns = new ActionLog(ACTION_LOG_AUTHENTICATION_DB_SNS, ACTION_LOG_COLLECTION_NAME_SNS, ACTION_LOG_USER_NAME_SNS,ACTION_LOG_PASSWORD_SNS, ACTION_LOG_HOST_SNS, ACTION_LOG_PORT_SNS);

} catch (Exception $e) {
    $_logger->error("DBへの接続エラー:" . $e);
}


?>