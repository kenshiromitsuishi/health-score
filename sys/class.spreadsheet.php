<?php
/*
* Spread Sheet関係のClass
*/
class SpreadSheet
{

    var $_spreadsheet;
    var $sheet;

    public function __construct()
    {
        try{
            // gcpの認証情報作成で作成した鍵ファイル(json)を定義
            $key = __DIR__ . '/../emoji-json-af033427546b.json';

            $client = new Google_Client();
            $client->setScopes([Google_Service_Sheets::SPREADSHEETS, Google_Service_Sheets::DRIVE]);
            $client->setAuthConfig($key);
            $this->sheet = new Google_Service_Sheets($client);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * @return array
     */
    public function batchGet($range)
    {
        try{
            $response = $this->sheet->spreadsheets_values->batchGet(SPREADSHEET_ID, $range, $options);
            $values = $response->getValueRanges();
            return $values;
        } catch (Exception $e){
            var_dump("スプシからの取得エラー：".$e);
            //TODO:$loggerでログを出力
        }
    }

    /**
     * 
     */
    public function outputAccess($sheetTitle, $values, $colnum=null, $index_log=null)
    {
        try{
            if(is_null($colnum)){
                $range = $sheetTitle."!A2";
            }else{
                $range = $sheetTitle."!{$colnum}{$index_log}";
            }
            $body = new \Google_Service_Sheets_ValueRange(
                [ 'values' => $values ]
            );
            $result = $this->sheet->spreadsheets_values->append(
                SPREADSHEET_ID, $range, $body, 
                [ 
                    'valueInputOption' => 'USER_ENTERED' 
                ]);
        } catch (Exception $e){
            var_dump("スプシ出力時のエラー：".$e);
            //TODO:$loggerでログを出力
        }
    }

    public function outputAccessBATCH($data)
    {
        try{    
            $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
                'valueInputOption' => 'USER_ENTERED',
                'data' => $data
                ]);
            $result = $this->sheet->spreadsheets_values->batchUpdate(SPREADSHEET_ID, $body);
        } catch (Exception $e){
            var_dump("スプシ出力時のエラー：".$e);
            //TODO:$loggerでログを出力
        }
    }
    
}