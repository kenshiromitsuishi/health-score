<?php
require("./config.php");

try{

    //変更すべき箇所
    $DB = SNS_DB;
    $SUMMARY = SNS_SUMMARY;
    $SCORE = SNS_SCORE;
    
    //$logs_month = $_actionLogger_km->getMonthlyLog_km(); //mongodbより、ユーザーIDとアクセス数をとってくる
    $logs_month = $_actionLogger_sns->getMonthlyLog_sns(); //mongodbより、ユーザーIDとアクセス数をとってくる
    
    //$logs_week = $_actionLogger_km->getWeeklyLog_km(); //mongodbより、ユーザーIDとアクセス数をとってくる
    $logs_week = $_actionLogger_sns->getWeeklyLog_sns(); //mongodbより、ユーザーIDとアクセス数をとってくる

    //$activeUsers_1 = $_actionLogger_km->getActiveUsers_km_1(); //mongodbより、ユーザーIDとユーザー数をとってくる。
    //$activeUsers_2 = $_actionLogger_km->getActiveUsers_km_2(); //mongodbより、ユーザーIDとIPアドレスをとってくる
    $activeUsers_1 = $_actionLogger_sns->getActiveUsers_sns_1(); //mongodbより、ユーザーIDとユーザー数をとってくる
    $activeUsers_2 = $_actionLogger_sns->getActiveUsers_sns_2(); //mongodbより、ユーザーIDとユーザー数をとってくる

    //var_dump($activeUsers_1);
    //exit;

    //週次集計対象のカラム取得
    $range = array(
      'ranges' => [
        $SUMMARY.'!A1:AH1', //SUMMARYのタイトル（colmunとして取得する）
        $DB.'!A:B', //DEV_LOGの企業ID, ユーザーID
        $SUMMARY.'!A:A', //DEV_SUMMARYの企業ID
      ]
    );
    $values = $_spreadsheet->batchGet($range);
    $weekday = $_db->getweekday();
    //var_dump($weekday);

    foreach($values[0]->values[0] as $index => $column){ //DEV_SUMMARYにて、「週次利用数◯」のタイトルを取得
      if($column == SUMMARY_TITLE["月次利用数"]){
        $alphabet_month = CONVERT_ALPHA_NUMRIC[$index+1]; //数値(回)のセルを取得する
      }
      if($column == $weekday){
        $alphabet_week = CONVERT_ALPHA_NUMRIC[$index+1]; //出力するセルのアルファベットを取得する。通常は+1s
      }
      if($column == SUMMARY_TITLE["アクティブユーザー数"]){
        $alphabet_activeUsers_1 = CONVERT_ALPHA_NUMRIC[$index+2]; //数①(回)のセルを取得する
        $alphabet_activeUsers_2 = CONVERT_ALPHA_NUMRIC[$index+3]; //数②(回)のセルを取得する
      }
    }

    $alphabet_week = "AL";
    //var_dump($alphabet_month);
    //var_dump($alphabet_week);
    //exit;

    //企業ごとのまとめ month
    $results_month = [];
    foreach($values[1]->values as $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($logs_month as $log_month){ //mongoより、ユーザーIDとアクセス数
            if($column[1] == $log_month["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
              $results_month[$column[0]] = $results_month[$column[0]] + $log_month["access_count"];
              }
          }
      }
      
    //企業ごとのまとめ week
    $results_week = [];
    foreach($values[1]->values as $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($logs_week as $log_week){ //mongoより、ユーザーIDとアクセス数
            if($column[1] == $log_week["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
              $results_week[$column[0]] = $results_week[$column[0]] + $log_week["access_count"];
              }
          }
      }

    //企業ごとのまとめ activeUser①
    $results_activeUsers_1 = [];
    foreach($values[1]->values as $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($activeUsers_1 as $activeUser){ //mongoより、ユーザーIDとアクセス数
            if($column[1] == $activeUser["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
              $results_activeUsers_1[$column[0]] = $results_activeUsers_1[$column[0]] + 1;
              }
          }
      }

    //企業ごとのまとめ activeUser②
    $results_activeUsers_2 = [];
    foreach($values[1]->values as $column){ //$column = DEV_LOGのユーザーIDと企業名
        foreach($activeUsers_2 as $activeUser){ //mongoより、ユーザーIDとアクセス数
            if($column[1] == $activeUser["id"]){ //DEV_LOGのユーザーIDとmongoからのユーザーID
              $results_activeUsers_2[$column[0]] = $results_activeUsers_2[$column[0]] + 1;
              }
          }
      }
      





    //SUMMARYシートに出力 month
    foreach($values[2]->values as $index => $column){ //$column = DEV_SUMMARYの企業ID
      foreach($results_month as $contract_group_id => $access_count){
        if($column[0] == $contract_group_id){
          $row = $index+1;
          $data_month[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SCORE."!L{$row}", //'range' => $SUMMARY."!{$alphabet_month}{$row}",
            'values' => [[$access_count]]
          ]);
          var_dump("企業ID：".$contract_group_id);
        }
      }
    }

    //SUMMARYシートに出力 week
    foreach($values[2]->values as $index => $column){ //$column = DEV_SUMMARYの企業ID
      foreach($results_week as $contract_group_id => $access_count){
        if($column[0] == $contract_group_id){
          $row = $index+1;
          $data_week[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet_week}{$row}",
            'values' => [[$access_count]]
          ]);
          var_dump("企業ID：".$contract_group_id);
        }
      }
    }

    //SUMMARYシートに出力 activeUser①
    foreach($values[2]->values as $index => $column){ //$column = DEV_SUMMARYの企業ID
      foreach($results_activeUsers_1 as $contract_group_id => $activeUsers){
        if($column[0] == $contract_group_id){
          $row = $index+1;
          $data_activeUsers_1[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet_activeUsers_1}{$row}",
            'values' => [[$activeUsers]]
          ]);
          var_dump("企業ID：".$contract_group_id);
        }
      }
    }

    //SUMMARYシートに出力 activeUser②
    foreach($values[2]->values as $index => $column){ //$column = DEV_SUMMARYの企業ID
      foreach($results_activeUsers_2 as $contract_group_id => $activeUsers){
        if($column[0] == $contract_group_id){
          $row = $index+1;
          $data_activeUsers_2[] = new \Google_Service_Sheets_ValueRange([
            'range' => $SUMMARY."!{$alphabet_activeUsers_2}{$row}",
            'values' => [[$activeUsers]]
          ]);
          var_dump("企業ID：".$contract_group_id);
        }
      }
    }

    $_spreadsheet->outputAccessBATCH($data_month);
    $_spreadsheet->outputAccessBATCH($data_week);
    $_spreadsheet->outputAccessBATCH($data_activeUsers_1);
    $_spreadsheet->outputAccessBATCH($data_activeUsers_2);
    var_dump("アクセスログ出力完了");
    
} catch(Exception $e){
   var_dump("エラーでやんす：".$e);
}

?>
