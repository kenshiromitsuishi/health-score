<?php
require("./config.php");

try{
    //dbより
    $users = $_kmdb->getUsersInfo(); //keywordmapdbからユーザー情報をとってくる

    //出力
    $values = array();
    $_logger->info("DBへ企業を追加");
    foreach($users as $user){
      [$user["contract_date"], $user["contract_sns_date_end"]] = $_db->FixcontractDate($user["contract_date"], $user["contract_sns_date_end"]);
      $product = $_db->productDiscrimination($user["seo"], $user["sns"], $user["km_or_kme"]);
      $contractType = $_db->contractTypeDiscrimination($user["seo"], $user["contract_type"]);
      [$activeUser_seo, $activeUser_sns] = $_db->defineActiveUser($user["user_count_seo"], $user["user_count_sns"], $user["login_bwsr"]);
      $values_all[] = array($user["contract_group_id"], $user["name"], $product, $contractType);
      if($user["km_or_kme"] == 1){
        $values_kme[] = array($user["contract_group_id"], $user["id"], $user["name"], $product, $user["contract_date"], $user["contract_sns_date_end"], $contractType, $activeUser_seo, CURRENT_TIMESTAMP);
        $_logger->info([$user["name"]]);
      }else if($user["seo"] == 1){
        $values_km[] = array($user["contract_group_id"], $user["id"], $user["name"], $product, $user["contract_date"], $user["contract_sns_date_end"], $contractType, $activeUser_seo, CURRENT_TIMESTAMP);
        $_logger->info([$user["name"]]);
      }
      if($user["sns"] == 1){
        $values_sns[] = array($user["contract_group_id"], $user["id"], $user["name"], $product, $user["contract_date"], $user["contract_sns_date_end"], $contractType, $activeUser_sns, CURRENT_TIMESTAMP);
        $_logger->info([$user["name"]]);
      }
    }
    
    //同じ企業IDで複数のユーザーIDを契約いただいている企業は、代表して一番小さいユーザーIDを残す、という処理
    /*
    $array2 = array_column($values_sns, 0); //指定のキーだけ抜き出す
    $array3 = array_unique($array2); //重複をなくす
    $array4 = array_values($array3); //消去分のインデックスを詰める
    array_multisort($array2, SORT_ASC, $values_sns);//並べ替え
    //var_dump($values_sns);
    //exit;
    var_dump(count($values_sns));

    $result = [];
    for($i = 0; $i < count($values_sns); $i++) {
      if($values_sns[$i][0] == $values_sns[$i-1][0]){
        //var_dump($values_sns[$i][0]);
        unset($values_sns[$i]);
      }
    }

    //var_dump($result);
    var_dump(count($values_sns));
    exit;*/

    //出力
    //$_spreadsheet->outputAccess(ALL_DB, $values_all); //出力
    //$_spreadsheet->outputAccess(KM_DB, $values_km); //出力
    //$_spreadsheet->outputAccess(KME_DB, $values_kme); //出力
    $_spreadsheet->outputAccess("KME_企業一覧_ver2", $values_kme); //出力
    
    $_logger->info("DB追加完了");

} catch(Exception $e){
   var_dump("エラーでやんす：".$e);
}


?>
