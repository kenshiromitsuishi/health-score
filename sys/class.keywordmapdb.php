<?php
class KeywordmapDB
{
    public $_kmdb;
    private const COOKIE_LENGTH = 80;
    private const COOKIE_EXPR = '/[\w\-\=\\\\\/#\$\%\.]+/u';


    public function __construct($host, $name, $user, $pass)
    {
        try {
            $this->_kmdb = new PDO('mysql:host=' . $host . ';dbname=' . $name . ';charset=utf8mb4', $user, $pass);
            $this->_kmdb->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (Exception $e) {
            throw $e;
        }
        
    }

    /**
     * @return array
     */
    public function getUsersInfo()
    {
        //$sql = "SELECT id, name, contract_group_id, contract_date, contract_sns_date_end, product_seo, product_sns_marketing, contract_type FROM user_login ORDER BY last_login DESC LIMIT 10;";
        //$sql = "SELECT id, name, contract_group_id, contract_date, contract_sns_date_end, product_seo, product_sns_marketing, contract_type FROM user_login WHERE status = 0 AND (contract_date is not null OR contract_sns_date_end is not null) ORDER BY id ASC;";
        $sql = "SELECT
                    ul.id,
                    ul.name,
                    ul.contract_group_id,
                    ul.contract_date,
                    ul.contract_sns_date_end,
                    ul.product_seo as seo,
                    ul.product_sns_marketing as sns,
                    ul.contract_type,
                    ul.login_bwsr,
                    cg.contract_group_type as km_or_kme,
                    cg.registered_user_count_seo as user_count_seo,
                    cg.registered_user_count_snsm as user_count_sns
                FROM
                    user_login ul
                INNER JOIN
                contract_group cg
                ON
                    ul.contract_group_id = cg.contract_group_id
                WHERE
                    ul.status = 0
                    AND (ul.contract_date > CURRENT_DATE()
	                    OR ul.contract_sns_date_end > CURRENT_DATE())
                    AND (ul.contract_date_start > '2022-05-01 00:00:00'
                        OR ul.contract_sns_date_start > '2022-05-01 00:00:00')
                    AND ul.contract_group_id not in (5, 8318, 12143, 12144, 12146, 12147, 731)
                    AND ul.name not like '%CINC%'
                    AND ul.name not like '%社内%'
                    AND ul.name not like '%開発%'
                    AND ul.name not like '%トライアル%'
                    AND ul.name not like '%テスト%'
                    AND ul.name not like '%黒澤%'
                    AND ul.name not like '%加藤優樹%'
                    AND ul.name not like '%アカウント%'
                    AND ul.name not like ''
                    AND (ul.contract_date is not null OR ul.contract_sns_date_end is not null)
                ORDER BY
                    id ASC;";

        $stmt = $this->_kmdb->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    
}